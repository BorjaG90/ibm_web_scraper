# IBM Web Scraper
Web Scraper written in Phyton3 for the web game [International Basketball Manager](http://es.ibasketmanager.com)

## Prerequisites
Need an account for the game

Need Python 3 and access to a MongoDB database

## Installing


Install the libraries:

`pip install beautifulsoup4`

`pip install requests`

`pip install pymongo`

## Deployment
### Built with
#### Libraries
* [BeautifulSoup4](https://www.crummy.com/software/BeautifulSoup/)
* [Requests](http://docs.python-requests.org/en/master/)

#### Tools
* [Visual Studio Code](https://code.visualstudio.com/)

## Author
* [Borja Gete](https://github.com/BorjaG90)

## License
